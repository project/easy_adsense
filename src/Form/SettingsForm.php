<?php

declare(strict_types=1);

namespace Drupal\easy_adsense\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Easy adsense settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'easy_adsense_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['easy_adsense.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Google Adsense Client'),
      '#prefix' => '<div>',
      '#description' => $this->t('This Client is unique for each site, and is in the form of ca-pub-xxxxxxxx.'),
      '#suffix' => '</div>',
    ];
    $form['wrapper']['client'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Client'),
      '#default_value' => $this->config('easy_adsense.settings')->get('client'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('easy_adsense.settings')
      ->set('client', $form_state->getValue('client'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
