(function (drupalSettings) {
  const gads = drupalSettings.gads;

  if (gads.client.length !== 0) {
    const script = document.createElement('script')
    script.async = true;
    script.crossOrigin = "anonymous"
    script.src = 'https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=' + gads.client
    script.type = 'text/javascript';
    document.getElementsByTagName('head')[0].appendChild(script);
  }

})(drupalSettings);
