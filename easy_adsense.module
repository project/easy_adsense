<?php

/**
 * @file
 * Primary module hooks for easy_adsense module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function easy_adsense_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name == 'help.page.easy_adsense') {
    $output = '';
    $output .= '<h2>' . t('About') . '</h2>';
    $output .= '<p>' . t('This module adds a simple integration with <a href=":url">Google Adsense</a>', [':url' => 'https://adsense.google.com/']) . '</p>';
    $output .= '<h2>' . t('Usage') . '</h2>';
    $output .= '<dl>';
    $output .= '<dt>' . t('After registering on Adsense, get the client id, it will be something like: <i>ca-pub-xxxxxxxxx</i>') . '</dt>';
    $output .= '<dt>' . t('With the client id you can go on the configuration page: <b>/admin/config/services/easy-adsense</b>') . '</dt>';
    $output .= '<dt>' . t('Fill the client id on the page and hit save') . '</dt>';
    $output .= '<dt>' . t('Now the google adsense script should be added on the <head> tag') . '</dt>';
    $output .= '</dl>';
    return $output;
  }
}

/**
 * Implements hook_page_attachments().
 */
function easy_adsense_page_attachments(array &$attachments) {
  $client = \Drupal::config('easy_adsense.settings')->get('client');

  // In case the user has not configured the client, return it.
  if (empty($client)) {
    return;
  }

  // Pass the client configuration to the drupal settings.
  $attachments['#attached']['drupalSettings']['gads'] = [
    'client' => $client,
  ];

  // Attach the library.
  $attachments['#attached']['library'][] = 'easy_adsense/gads';
}
