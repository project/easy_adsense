## INTRODUCTION

The easy_adsense module is a lightweight module to integrate your site to google adsense.

## REQUIREMENTS

Drupal 9 or 10

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Create your google adsense account
- Configure your client on: Admin > Config > Services > Easy Adsense(admin/config/services/easy-adsense)
- Using the client code provided by adsense, fill the field and hit save
- Export the configurations in order to keep the data

That's it, your site should have necessary scripts for the google ads on the <head> tag.

## MAINTAINERS

Current maintainers:

- Murilo Pucci (murilohp) - https://www.drupal.org/u/murilohp
