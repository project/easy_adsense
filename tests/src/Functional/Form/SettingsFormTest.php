<?php

declare(strict_types=1);

namespace Drupal\Tests\google_tag\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * @coversDefaultClass \Drupal\easy_adsense\Form\SettingsForm
 * @group easy_adsense
 */
final class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The user tp be tested.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'easy_adsense',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->user = $this->drupalCreateUser(['administer site configuration']);
  }

  /**
   * Test the main form and the integration with javascript.
   */
  public function testIntegration(): void {
    $this->drupalLogin($this->user);

    // Go to the admin form.
    $this->drupalGet('/admin/config/services/easy-adsense');
    $this->assertSession()->statusCodeEquals(200);

    // Save a dummy client.
    $edit = [];
    $edit['client'] = 'ca-pub-xxxxxxxxx';
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');

    // Evaluate the client being passed to drupal settings.
    $drupalSettings = $this->getDrupalSettings();
    $this->assertSame([
      'client' => $edit['client'],
      'settings' => [],
    ], $drupalSettings['gads']);

  }

}
